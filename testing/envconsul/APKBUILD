# Contributor: Gennady Feldman <gena01@gmail.com>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=envconsul
pkgver=0.12.0
pkgrel=0
pkgdesc="Read and set environmental variables for processes from Consul."
url="https://www.consul.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/envconsul/archive/v$pkgver.tar.gz"

build() {
	export CGO_ENABLED="0"
	mkdir -p pkg/linux-$CARCH
	go build -v -o pkg/linux-$CARCH/$pkgname
}

check() {
	go test -timeout=30s -parallel=20 -failfast
}

package() {
	# Main binary
	install -m755 -D pkg/linux-$CARCH/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
8bf9eb5a04272d634276d3b1f1d29e15b768a074b10af6d31cdd436d071da05f8298935945f8df02204857d8e340df0e43b1cef5ccafe3165ac1fe697391521e  envconsul-0.12.0.tar.gz
"
